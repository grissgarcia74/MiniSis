/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umss.sisii.minisis.resource;

import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.umss.sisii.minisis.database.DBManager;
import org.umss.sisii.minisis.model.Score;

/**
 *
 * @author PC_
 */
@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {
    
    private final DBManager manager = DBManager.getInstance();
    
    @POST
    public List<Score> getScores(int userId){
        manager.getScores(userId);
        return null;
    }
}
