(function () {
    'use strict';
    
    angular.module('app.task', []).service('TaskService', TaskService);
    
    TaskService.$inject = ['$http'];
            
    function TaskService($http) {
        var service = {
            getScores: getScores
        };
        
        function getScores(userId, onSuccess, onError){
            $http({
                method: 'POST',
                url: 'http://localhost:8080/minisissII/api/task',
                data: userId
            }).then(onSuccess, onError);
        }
        return service;
    }
})();